﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class RedisDbInitializer : IDbInitializer
    {
        private readonly IConnectionMultiplexer _redis;
        private readonly IDatabase _db;

        public RedisDbInitializer(IConnectionMultiplexer redis)
        {
            _redis = redis;
            _db = redis.GetDatabase();
        }

        public void InitializeDb()
        {
            foreach (var p in FakeDataFactory.Preferences)
            {
                var serialEntity = JsonSerializer.Serialize(p);
                _db.HashSet($"{p.GetType().Name}", new HashEntry[]
                    { new HashEntry(p.Id.ToString(),serialEntity)});

            }

            foreach (var c in FakeDataFactory.Customers)
            {
                var serialEntity = JsonSerializer.Serialize(c);
                _db.HashSet($"{c.GetType().Name}", new HashEntry[]
                    { new HashEntry(c.Id.ToString(),serialEntity)});
            }
        }
    }
}
