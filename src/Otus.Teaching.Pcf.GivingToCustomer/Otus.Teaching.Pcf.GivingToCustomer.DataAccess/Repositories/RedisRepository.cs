﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class RedisRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly IConnectionMultiplexer _redis;
        private readonly IDatabase _db;

        public RedisRepository(IConnectionMultiplexer redis)
        {
            _redis = redis;
            _db = redis.GetDatabase();
        }

        public async Task AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentOutOfRangeException(nameof(entity));
            }

            var serialEntity = JsonSerializer.Serialize(entity);

            await _db.HashSetAsync($"{entity.GetType().Name}",new HashEntry[]
                    { new HashEntry(entity.Id.ToString(),serialEntity)});

        }

        public async Task DeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentOutOfRangeException(nameof(entity));
            }

            await _db.HashDeleteAsync($"{entity.GetType().Name}", entity.Id.ToString());
            
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var name = typeof(T).Name;
            var completeHash = await _db.HashGetAllAsync(name);
            if (completeHash.Length > 0)
            {
                var obj = Array.ConvertAll(completeHash, val => JsonSerializer.Deserialize<T>(val.Value)).ToList();

                return obj;
            }
            return null;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var name = typeof(T).Name;
            var entityString = await _db.HashGetAsync(name, id.ToString());

            if (string.IsNullOrEmpty(entityString))
            {
                throw new ArgumentOutOfRangeException();            
            }
            else
            {
                return JsonSerializer.Deserialize<T>(entityString);
            }
        }

        public async Task UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentOutOfRangeException(nameof(entity));
            }

            var serialEntity = JsonSerializer.Serialize<T>(entity);

            await _db.HashSetAsync($"{entity.GetType().Name}", new HashEntry[]
                    { new HashEntry(entity.Id.ToString(),serialEntity)});
        }

        #region NonImplemented
        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
